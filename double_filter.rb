puts "Text preparation begins now!"

# CLI arguments
if ARGV[0] == nil
  puts "ABORT: Filename required!"
  abort
end

input_file = ARGV[0]
output_file = if ARGV[1] then ARGV[1] else "outfile.txt" end
splitter = if ARGV[2] then ARGV[2] else " " end

# Check, if file exists. If not, prepend path to work dir and try again.
if !File.file?(input_file) 
  new_path = "#{Dir.pwd}/#{input_file}"
  if File.file?(new_path)
    input_file = new_path 
  else
    puts "ABORT: #{input_file} not found at #{Dir.pwd}."
    abort
  end
end

# Check if path is absolute. If not, prepend path to work dir.
if /^\//.match(output_file)
  output_file = Dir.pwd + "/" + output_file
end

puts "Reading #{input_file}..."

hash = Hash.new()
input = File.new( input_file, "r" )

while line = input.gets

  line = line.chomp
  if line.empty? then next end
  
  parts = line.split(splitter)
  string = ""
  i = 0
  
  parts.each do | part |
    string += part + " " if i > 0
    i += 1
  end

  key = if parts[0].empty? then "EMPTY_STRING" else parts[0] end

  if hash[ key ] == nil
    hash[ key ] = string
  else
    hash[ key ] << " | " + string
  end

end

input.close

puts "Writing to #{output_file}..."

output = File.new( output_file, "w" )
hash = hash.sort_by { | key, value | key }.to_h

hash.each do | key, value |
  string = "#{key}: #{value}"
  output.puts(string)
end

output.close

puts "Finished!"
