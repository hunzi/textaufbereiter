# Text-Filter-Script

Einfaches Script, um Wiederholungen in Zeilen aus Textdateien zu filtern, möglichst ohne Informationsverlust. 

ruby double\_filter.rb input\_file\_name output\_file\_name splitter

  * input\_file -> **required** zu filternde Datei 
  * output\_file -> _optional_ Datei, in welche geschrieben wird. Standard: outfile.txt
  * splitter -> _optional_ Zeichen, an welchem der String getrennt werden soll. Standard: " "

Vorgehen: 
Das Script filtert eine Datei Zeile für Zeile. Die Strings werden gesplittet und der erste Part des Strings wird zu einem Key in einem Hash. Alle weitere Einträge, die mit diesem Part beginnen, werden diesem Key zugeordnet. Anschließend wird das Ergebnis in eine Datei geschrieben.


